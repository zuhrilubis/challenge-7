        const gunting = document.getElementById('guntinguser');
        const batu = document.getElementById('batuuser');
        const kertas = document.getElementById('kertasuser');
        const refreshbutton = document.getElementById('boxrefresh');
        
        const guntingkanan = document.getElementById("guntingcom");
        const batukanan = document.getElementById("batucom");
        const kertaskanan = document.getElementById("kertascom");
        const reset = document.getElementById("buttontest");

        const clickfree = () => {
          gunting.classList.add("nonaktifkan");
          batu.classList.add("nonaktifkan");
          kertas.classList.add("nonaktifkan");
          };

        const komputermemilih = () => {
            const pilihanyangada = ["guntingcom", "batucom", "kertascom"];
            const rndInt = Math.floor(Math.random() * 3);
            const pilihankomputer = pilihanyangada[rndInt];
            kasihBackground(pilihankomputer);
            commemilih = pilihanyangada[rndInt];
          };

        const kasihBackground = (sesuatu) => {
            const pilihanrandom = document.getElementById(sesuatu);
            pilihanrandom.style.backgroundColor = "lavender";
            pilihanrandom.style.borderRadius = "15%";
          };

        gunting.onclick = () => {
          gunting.style.backgroundColor = 'lavender';
          gunting.style.borderRadius = '15%';
          clickfree(); 
          komputermemilih();
          usermemilih = "guntinguser";
          hasiltanding (); 
        }
    
        batu.onclick = () => {
          batu.style.backgroundColor = 'lavender';
          batu.style.borderRadius = '15%';
          clickfree();
          komputermemilih();
          usermemilih = "batuuser";
          hasiltanding ();
        }

        kertas.onclick = () => {
          kertas.style.backgroundColor = 'lavender';
          kertas.style.borderRadius = '15%';
          clickfree(); 
          komputermemilih();
          usermemilih = "kertasuser";
          hasiltanding (); 
        }
        
        class Penentuan {
            imbang = () => {
            const badantengah = document.getElementById('cover');
            const vs = document.getElementById('standby');
            const adiv = document.createElement ('div');
            adiv.setAttribute('id', 'draw');
            adiv.innerHTML = "<p><strong>DRAW</strong></p>";
            badantengah.appendChild(adiv);
            vs.style.display = "none";
            }

            menang = () => {
              const badantengah = document.getElementById('cover');
              const vs = document.getElementById('standby');
              const adiv = document.createElement ('div');
              adiv.setAttribute('id', 'userwin');
              adiv.innerHTML = "<p><strong>PLAYER 1<br>WIN</br></strong></p>";
              badantengah.appendChild(adiv);
              vs.style.display = "none";
            }

            kalah = () => {
              const badantengah = document.getElementById('cover');
              const vs = document.getElementById('standby');
              const adiv = document.createElement ('div');
              adiv.setAttribute('id', 'comwin');
              adiv.innerHTML = "<p><strong>COM<br>WIN</br></strong></p>";
              badantengah.appendChild(adiv);
              vs.style.display = "none";
            }
        }

        const result = new Penentuan ()
       
         const hasiltanding = () => {
            if (usermemilih === "guntinguser" && commemilih === "guntingcom") {
              result.imbang ();
            } else if (usermemilih === "batuuser" && commemilih === "batucom") {
              result.imbang ();
            } else if (usermemilih === "kertasuser" && commemilih === "kertascom") {
              result.imbang ();
            } else if (usermemilih === "guntinguser" && commemilih === "kertascom") {
              result.menang ();
            } else if (usermemilih === "batuuser" && commemilih === "guntingcom") {
              result.menang ();
            } else if (usermemilih === "kertasuser" && commemilih === "batucom") {
              result.menang ();
            } else {
              result.kalah ();
            };
          };

          const siapmain = () => {
            const badantengah = document.getElementById('cover');
            const adiv = document.createElement ('div');
            adiv.setAttribute('id', 'standby');
            adiv.innerHTML = "<p><strong>VS</strong></p>";
            badantengah.appendChild(adiv); 
          }

        refreshbutton.onclick = () => {
            gunting.classList.remove("nonaktifkan");
            batu.classList.remove("nonaktifkan");
            kertas.classList.remove("nonaktifkan");
            gunting.style.removeProperty('background-color');
            batu.style.removeProperty('background-color');
            kertas.style.removeProperty('background-color');
            guntingkanan.style.removeProperty('background-color');
            batukanan.style.removeProperty('background-color');
            kertaskanan.style.removeProperty('background-color');
            siapmain ();
        }  

          
    