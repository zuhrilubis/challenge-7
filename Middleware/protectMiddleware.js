const protection = async (req, res, next) => {
  const { userId } = req.params;
  const idRequest = req.dataUserDariToken.id;

  if (idRequest.toString() !== userId) {
    return res.send({ message: "Sorry, User unable to authorized" });
  }

  next();
};

module.exports = protection;
