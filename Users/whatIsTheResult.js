const theGameResult = (choiceOne, choiceTwo) => {
  if (
    (choiceOne === "batu" && choiceTwo === "batu") ||
    (choiceOne === "gunting" && choiceTwo === "gunting") ||
    (choiceOne === "kertas" && choiceTwo === "kertas")
  ) {
    return ["draw", "draw"];
  } else if (
    (choiceOne === "batu" && choiceTwo === "gunting") ||
    (choiceOne === "gunting" && choiceTwo === "kertas") ||
    (choiceOne === "kertas" && choiceTwo === "batu")
  ) {
    return ["win", "lose"];
  } else if (
    (choiceOne === "batu" && choiceTwo === "kertas") ||
    (choiceOne === "gunting" && choiceTwo === "batu") ||
    (choiceOne === "kertas" && choiceTwo === "gunting")
  ) {
    return ["lose", "win"];
  } else {
    return ["error", "error"];
  }
};

function ComputerChoice() {
  const choices = ["batu", "gunting", "kertas"];
  const randomIndex = Math.floor(Math.random() * choices.length);
  return choices[randomIndex];
}

module.exports = { theGameResult, ComputerChoice };
