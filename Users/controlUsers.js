const ModelUsers = require("./modelUsers");
const jwtRule = require("jsonwebtoken");
const { theGameResult, ComputerChoice } = require("./whatIsTheResult");
// require("dotenv").config();

class ControlUsers {
  // mendaftarkan user baru
  storeNewUsers = async (req, res) => {
    const daftarData = req.body;

    // cek validitas data yang diinput
    if (daftarData.userName === undefined || daftarData.userName === "") {
      res.statusCode = 404;
      return res.json({ message: "Username is unavailable" });
    }
    if (daftarData.email === undefined || daftarData.email === "") {
      res.statusCode = 404;
      return res.json({ message: "Email is unavailable" });
    }
    if (daftarData.password === undefined || daftarData.password === "") {
      res.statusCode = 404;
      return res.json({ message: "Password is unavailable" });
    }

    // mendeteksi apakah user sudah terdaftar
    const usedData = await ModelUsers.isUserIdentified(daftarData);
    if (usedData) {
      res.statusCode = 404;
      return res.json({ message: "Username or Email already exist" });
    }

    ModelUsers.storeNewUsers(daftarData);

    res.json({ message: "New user is being added!" });
  };

  // dapatkan semua data users
  getAllUsers = async (req, res) => {
    const allUsers = await ModelUsers.getAllUsers();

    // SELECT * FROM users
    return res.json(allUsers);
  };

  // verifikasi login
  verifyLogin = async (req, res) => {
    const { userName, password } = req.body;

    // cek validitas data yang diinput
    if (userName === undefined || userName === "") {
      res.statusCode = 404;
      return res.json({ message: "Write your username please" });
    }

    if (password === undefined || password === "") {
      res.statusCode = 404;
      return res.json({ message: "Write your Password please" });
    }

    const dataLogin = await ModelUsers.verifyLogin(userName, password);

    // jika user valid
    if (dataLogin) {
      // generate JWT
      const token = jwtRule.sign(
        { ...dataLogin, role: "player" },
        "CobaanTujuh100114ZhRidhoanhu1!*",
        { expiresIn: "3h" }
      );
      return res.json({ accessToken: token });
    } else {
      res.statusCode = 404;
      return res.json({
        message: "Sorry, you are unregistered user, register first please",
      });
    }
  };

  // update biodata user
  updateUserBio = async (req, res) => {
    // dapatkan id user dari param
    const { userId } = req.params;

    // dapatkan update data dari body
    const { fullName, phoneNumber, address } = req.body;

    // cek apakah user sudah terdaftar
    try {
      const registeredUser = await ModelUsers.getOneUser(userId);
      if (!registeredUser) {
        res.statusCode = 404;
        return res.json({ message: "User Id is unregistered" });
      }

      // jika user telah terdaftar
      if (registeredUser) {
        // cek validitas data yang diinput
        if (fullName === undefined || fullName === "") {
          res.statusCode = 404;
          return res.json({ message: "Write your full name please" });
        }
        if (address === undefined || address === "") {
          res.statusCode = 404;
          return res.json({ message: "Write your address please" });
        }
        if (phoneNumber === undefined || phoneNumber === "") {
          res.statusCode = 404;
          return res.json({ message: "Write your phone number please" });
        }

        await ModelUsers.updateUserBio(fullName, phoneNumber, address, userId);

        return res.json({
          message: `Biodata user Id: ${userId} is being updated`,
        });
      }
    } catch (error) {
      res.statusCode = 500;
      return res.json({ messages: "There is something wrong in request" });
    }
  };

  // menampilan data detail single user
  getOneUser = async (req, res) => {
    // dapatkan id user dari param
    const { userId } = req.params;
    try {
      const userData = await ModelUsers.getOneUser(userId);
      if (userData) {
        return res.json(userData);
      } else {
        res.statusCode = 404;
        return res.json({ message: `User id un identified : ${userId}` });
      }
    } catch (error) {
      res.statusCode = 500;
      return res.json({ messages: "There is something wrong in request" });
    }
  };

  // menambahkan history permainan
  storeGameHistory = async (req, res) => {
    // dapatkan id user dari param
    const { userId } = req.params;
    const { status } = req.body;
    try {
      const usedGameId = await ModelUsers.isUserPlayer(userId);
      if (!usedGameId) {
        res.statusCode = 404;
        return res.json({ message: "User Id is undefined" });
      }

      ModelUsers.storeUserGames(userId, status);

      res.json({ message: "New game history is being added!" });
    } catch {
      res.statusCode = 500;
      return res.json({ messages: "There is something wrong in request" });
    }
  };

  // menampilkan history permainan single user
  getOneUserGameHistory = async (req, res) => {
    // dapatkan id user dari param
    const { userId } = req.params;
    try {
      const userHistory = await ModelUsers.getGameHistories(userId);
      if (userHistory) {
        return res.json(userHistory);
      } else {
        res.statusCode = 404;
        return res.json({ message: `User id un identified : ${userId}` });
      }
    } catch {
      res.statusCode = 500;
      return res.json({ messages: "There is something wrong in request" });
    }
  };

  // API Tambahan
  //create new Game Room
  createGameRoom = async (req, res) => {
    const { roomName, playerOneChoice } = req.body;
    const playerOneId = req.dataUserDariToken.id;

    try {
      const roomNameValidation = await ModelUsers.roomNameValidation(roomName);

      if (roomNameValidation) {
        res.statusCode = 404;
        return res.json({
          message: "Create other name room, the room created already exist",
        });
      }

      await ModelUsers.createGameRoom(roomName, playerOneId, playerOneChoice);

      return res.send({
        message: `Success Record Room by User with ID : ${playerOneId}`,
      });
    } catch {
      res.statusCode = 500;
      return res.json({ messages: "There is something wrong in request" });
    }
  };

  //mendapatkan data seluruh room
  getAllGameRooms = async (req, res) => {
    try {
      const allGameRoom = await ModelUsers.getAllGameRoom();
      return res.json(allGameRoom);
    } catch {
      res.statusCode = 500;
      return res.json({ messages: "There is something wrong in request" });
    }
  };

  //mendapatkan Single Room Detail
  getOneRoomGame = async (req, res) => {
    // dapatkan id room game dari param
    const { roomId } = req.params;

    try {
      const oneRoomGame = await ModelUsers.oneRoomGameDetail(roomId);

      if (!oneRoomGame) {
        return res.status(404).send({ message: "Room game is not found" });
      }
      return res.json(oneRoomGame);
    } catch {
      res.statusCode = 500;
      return res.json({ messages: "There is something wrong in request" });
    }
  };

  //Update Single Room Game by player two
  updateOneRoomGame = async (req, res) => {
    const { roomId } = req.params;
    const { playerTwoChoice } = req.body;
    const playerTwoId = req.dataUserDariToken.id;

    try {
      //Get Room Detail
      const oneRoomGame = await ModelUsers.oneRoomGameUpdate(roomId);

      //verifikasi player one
      if (oneRoomGame.playerOneId === playerTwoId) {
        return res
          .status(400)
          .json({ messages: "It is your room, room owner can't fill second choice"});
      }

      //verifikasi room already completed
      if (oneRoomGame.roomStatus === "Completed") {
        return res.status(400).json({ messages: "Room is all complete, try an available room"});
      }

      //Input data player two
      await ModelUsers.updatePlayerTwoData(
        playerTwoChoice,
        playerTwoId,
        roomId
      );

      //create variable for update the playing
      const choieceOne = oneRoomGame.playerOneChoice;
      const choiceTwo = playerTwoChoice;

      //input varriable to find the winner
      const [playerOneResult, playerTwoResult] = theGameResult(
        choieceOne,
        choiceTwo
      );

      //Input result to database
      await ModelUsers.updateRoomGameResult(
        playerOneResult,
        playerTwoResult,
        roomId
      );

      return res.json({
        message: "Record History Success",
        "Player One Choice": choieceOne,
        "Player Two Choice": choiceTwo,
        "Player One ": playerOneResult,
        "Player Two ": playerTwoResult,
      });
    } catch (error) {
      console.log(error);
      return res
        .status(500)
        .json({ message: "Something Error, the room ID is not exist" });
    }
  };

  // mendapatkan seluruh history game per user
  getSingleGameHistory = async (req, res) => {
    try {
      const idPlayer = req.dataUserDariToken.id;

      // find singe room detail by ID
      const allGameHistory = await ModelUsers.getRoomDetail(idPlayer);
      return res.json(allGameHistory);
    } catch {
      res.statusCode = 500;
      return res.json({ messages: "There is something wrong in request" });
    }
  };

  // pertandinga Player vs Computer
  createRoomPlayerVsComputer = async (req, res) => {
    const { playerOneChoice } = req.body;
    const playerOne = req.dataUserDariToken.id;

    try {
      const roomName = "VS COM";
      const playerTwoChoice = ComputerChoice(); // ambil dari whatIsTheResult
      const [playerOneResult, playerTwoResult] = theGameResult(
        playerOneChoice,
        playerTwoChoice
      );

      await ModelUsers.createRoomPlayerVsComputer(
        roomName,
        playerOneChoice,
        playerTwoChoice,
        playerOne,
        playerOneResult,
        playerTwoResult
      );

      return res.json({
        message: "The Room are successfully created",
        Player: playerOneChoice,
        Computer: playerTwoChoice,
        playerOneResult: playerOneResult,
        playerTwoResult: playerTwoResult,
      });
    } catch {
      res.statusCode = 500;
      return res.json({ messages: "There is something wrong in request" });
    }
  };
}

module.exports = new ControlUsers();
