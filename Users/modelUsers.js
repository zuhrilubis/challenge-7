const md5 = require("md5");
const db = require("../db/models");
const { Op } = require("sequelize");

const userList = [];

class ModelUsers {
  // Mendaftarkan user baru
  storeNewUsers = (daftarData) => {
    db.User.create({
      userName: daftarData.userName,
      email: daftarData.email,
      password: md5(daftarData.password),
    });
  };

  // mendeteksi apakah user sudah terdaftar
  isUserIdentified = async (daftarData) => {
    const usedData = await db.User.findOne({
      where: {
        [Op.or]: [
          { userName: daftarData.userName },
          { email: daftarData.email },
        ],
      },
    });

    if (usedData) {
      return true;
    } else {
      return false;
    }
  };

  // mendapatkan data seluruh user
  getAllUsers = async () => {
    const dataUsers = await db.User.findAll({
      include: [db.UserBio, db.GameHistory],
    });

    // SELECT * FROM users
    return dataUsers;
  };

  // verify login
  verifyLogin = async (userName, password) => {
    const dataLogin = await db.User.findOne({
      where: { userName: userName, password: md5(password) },
      attributes: { exclude: ["password"] },
      raw: true,
    });

    return dataLogin;
  };

  //update biodata user
  updateUserBio = async (fullName, address, phoneNumber, userId) => {
    return await db.UserBio.upsert({
      id: userId,
      fullName: fullName,
      phoneNumber: phoneNumber,
      address: address,
      userId: userId,
    });
  };

  // menampilan data detail single user
  getOneUser = async (userId) => {
    return await db.User.findOne({
      include: [db.UserBio],
      where: { id: userId },
    });
  };

  // menambahkan history permainan
  storeUserGames = async (userId, status) => {
    await db.GameHistory.create({
      userId: userId,
      status: status,
    });
  };

  //verivikasi data user yang bermain
  isUserPlayer = async (userId) => {
    const usedGameId = await db.User.findOne({
      where: { id: userId },
    });

    if (usedGameId) {
      return true;
    } else {
      return false;
    }
  };

  // menampilkan history permainan single user
  getGameHistories = async (userId) => {
    return await db.User.findOne({
      where: { id: userId },
      include: [db.GameHistory],
    });
  };

  // for New API
  // validasi nama room
  roomNameValidation = async (roomName) => {
    try {
      return await db.GameRoom.findOne({
        where: { roomName: roomName },
      });
    } catch (error) {
      throw new Error(
        `Something wrong, Failed to check Room Name : ${error.message}`
      );
    }
  };

  // cretar Room Game
  createGameRoom = async (roomName, playerOneId, playerOneChoice) => {
    try {
      const gameRoom = await db.GameRoom.create({
        roomName: roomName,
        playerOneChoice: playerOneChoice,
        playerOneId: playerOneId,
        roomStatus: "Available",
      });
      return gameRoom;
    } catch (error) {
      throw new Error(
        `Something wrong, Failed to create new Room : ${error.message}`
      );
    }
  };

  // mendapatkan all rooms
  getAllGameRoom = async () => {
    try {
      const roomList = await db.GameRoom.findAll({
        include: [
          {
            model: db.User,
            as: "playerOne",
            attributes: ["id", "userName"],
          },
          {
            model: db.User,
            as: "playerTwo",
            attributes: ["id", "userName"],
          },
        ],
      });
      return roomList;
    } catch (error) {
      throw new Error(`Failed to get all Room Data : ${error.message}`);
    }
  };

  // mendapatkan Single Room Game Detail
  oneRoomGameDetail = async (roomId) => {
    try {
      const oneRoomDetiles = await db.GameRoom.findOne({
        where: {
          id: roomId,
        },
        include: [
          {
            model: db.User,
            as: "playerOne",
            attributes: ["id", "userName"],
          },
          {
            model: db.User,
            as: "playerTwo",
            attributes: ["id", "userName"],
          },
        ],
        attributes: { exclude: ["playerTwoChoice", "playerOneChoice"] },

        raw: true,
      });
      return oneRoomDetiles;
    } catch (error) {
      throw new Error(`Failed to get room game detail : ${error.message}`);
    }
  };

  // detail room game for update
  oneRoomGameUpdate = async (roomId) => {
    try {
      const roomSingleList = await db.GameRoom.findOne({
        where: { id: roomId },
        include: [
          {
            model: db.User,
            as: "playerOne",
            attributes: ["id", "userName"],
          },
          {
            model: db.User,
            as: "playerTwo",
            attributes: ["id", "userName"],
          },
        ],

        raw: true,
      });
      return roomSingleList;
    } catch (error) {
      throw new Error(`Failed to get room detail : ${error.message}`);
    }
  };

  // update data player two
  updatePlayerTwoData = async (playerTwoChoice, playerTwoId, roomId) => {
    try {
      const roomIdInt = parseInt(roomId);
      const playerTwoUpdate = await db.GameRoom.update(
        {
          playerTwoChoice: playerTwoChoice,
          playerTwoId: playerTwoId,
        },
        { where: { id: roomIdInt } }
      );
      return playerTwoUpdate;
    } catch (error) {
      throw new Error(`Failed to input player2 detail : ${error.message}`);
    }
  };

  // Update Room result from player two choice
  updateRoomGameResult = async (playerOneResult, playerTwoResult, roomId) => {
    const roomIdInt = parseInt(roomId);
    // console.log(roomIdInt);
    try {
      const playerTwoUpdate = await db.GameRoom.update(
        {
          playerOneResult: playerOneResult,
          playerTwoResult: playerTwoResult,
          roomStatus: "Completed",
        },
        { where: { id: roomIdInt } }
      );
      return playerTwoUpdate;
    } catch (error) {
      throw new Error(`Failed to update game room data : ${error.message}`);
    }
  };

  //mendapatkan single user all game history
  getRoomDetail = async (idPlayer) => {
    try {
      const roomList = await db.GameRoom.findAll({
        attributes: [
          "roomName",
          "updatedAt",
          "playerOneId",
          "playerTwoId",
          "playerOneResult",
          "playerTwoResult",
          "roomStatus",
        ],
        where: {
          [Op.or]: [{ playerOneId: idPlayer }, { playerTwoId: idPlayer }],
        },
        raw: true,
      });

      return roomList;
    } catch (error) {
      throw new Error(`Failed to retrieve all game history : ${error.message}`);
    }
  };

  // Create Room Player vs Computer
  createRoomPlayerVsComputer = async (
    roomName,
    playerOneChoice,
    playerTwoChoice,
    playerOne,
    playerOneResult,
    playerTwoResult
  ) => {
    try {
      const gameRoomVsCom = await db.GameRoom.create({
        roomName,
        playerOneChoice,
        playerOneId: playerOne,
        playerTwoChoice,
        playerTwoId: 2, // make playerTwoId as ID Computer
        playerOneResult,
        playerTwoResult,
        roomStatus: "Completed", // change status room to "Completed"
      });
      return gameRoomVsCom;
    } catch (error) {
      throw new Error(
        `Failed to create game room with computer : ${error.message}`
      );
    }
  };
}

module.exports = new ModelUsers();
